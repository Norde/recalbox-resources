#!/bin/sh
#savegames-to-nextcloud.sh - Savegame backups to Nextcloud

#Debug log (/recalbox/share/system/save-to-next.log)
#Uncomment lines below to use :
#exec 3>&1 4>&2
#trap 'exec 2>&4 1>&3' 0 1 2 3
#exec 1>/recalbox/share/system/save-to-next.log 2>&1

# -----------------------------------
# CONFIG (EDIT/FILL ALL VARIABLES)
# -----------------------------------
# directory settings
BACKUP_DIR='/recalbox/share/saves_backup'
SAVES_DIR='/recalbox/share/saves'

# backup retention time (day)
BACKUP_RT='7'
# number of (most recent) backup files to keep
BACKUP_NB='10'

#owncloud/nextcloud public share settings, ex: (https://my-cloud-server.domain/index.php/s/ToKeN)
# -> see readme for owncloud/nextloud setup
CLOUD_URL='https://my-cloud-server.domain/'
CLOUD_PUBSUFFIX='public.php/webdav'
CLOUD_HEADER='X-Requested-With: XMLHttpRequest'
CLOUD_FOLDERTOKEN='ToKeN'
CLOUD_FOLDERPASSWORD='my_str0ng_p@ssword'
# ----------
# END CONFIG
# ----------


# ------
# BACKUP
# ------
#remount root to get write access
mount -o remount,rw /

# create backup dir if it's not here already
mkdir -p ${BACKUP_DIR}

# get clean date
date=$(date +%Y-%m-%d_%Hh%Mm%Ss) # 2020-02-02_20h02m02s

# savegames backup
##################

# make a tar a tar preserving ownership and permission of the uploads folder
tar -cvpf recalbox-saves-${date}.backup.tar ${SAVES_DIR}

# compress tar file with 7z
7zr a archive.tar.7z -mx=9 recalbox-saves-${date}.backup.tar
# delete tar file
rm recalbox-saves-${date}.backup.tar

# move the file to a local backup dir
mv archive.tar.7z ${BACKUP_DIR}/recalbox-saves-${date}.backup.tar.7z


# -----------------
# CLEAR OLD BACKUPS
# -----------------
# delete backups older than {BACKUP_RT} days except {BACKUP_NB} most recent
# retention time (days) and number of backups to keep has to be defined in CONFIG part
#######################
find ${BACKUP_DIR}/ -type f -mtime +${BACKUP_RT} | grep -v '/$' | tail -n +${BACKUP_NB} | tr '\n' '\0' | xargs -0 rm --

# -------------------------
# SEND BACKUPS TO NEXTCLOUD
# -------------------------
# send current backup to owncloud/nextcloud "file drop"
# this requires to have owncloud/nextcloud public share configured with password
# -> see readme
#######################
curl -T ${BACKUP_DIR}/recalbox-saves-${date}.backup.tar.7z -u "${CLOUD_FOLDERTOKEN}":"${CLOUD_FOLDERPASSWORD}" -H "${CLOUD_HEADER}" "$CLOUD_URL/$CLOUD_PUBSUFFIX/recalbox-saves-${date}.backup.tar.7z"


# delete distant backup on owncloud/nextcloud (using file system tag)
# -> see readme
